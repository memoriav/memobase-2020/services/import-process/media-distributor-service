/*
 * Media File Distributor
 * Copyright (C) 2021  Memoriav
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

use crate::endpoints::Endpoint;
use crate::media_folder_utils::MediaFileCache;
use anyhow::{anyhow, Context as AContext, Result};
use hyper::http::header;
use hyper::service::Service;
use hyper::{Body, Request, Response, StatusCode};
use log::{error, info, warn};
use std::fs::File;
use std::future::Future;
use std::io::{BufReader, Read};
use std::pin::Pin;
use std::sync::{Arc, Mutex};
use std::task::{Context, Poll};
use std::time::Duration;

/// A [`hyper::service::Service`] implementation which wraps the [`MediaFileCache`]
pub struct Svc {
    /// [`MediaFileCache`]
    pub media_cache: Arc<Mutex<MediaFileCache>>,
    /// Path to root directory of media tree
    pub base_dir: String,
    /// Duration after which the cache becomes invalid and its refresh is enforced
    pub cache_timeout: Duration,
    /// Duration after which a collection cache is susceptible to a refresh
    pub partial_cache_timeout: Duration,
}

impl Svc {
    /// Refreshes the overall cache if necessary
    fn refresh_cache_if_outdated(&mut self) -> Result<()> {
        if let Ok(mut x) = self.media_cache.lock() {
            if x.is_outdated(&self.cache_timeout) {
                info!("Media cache is outdated! Regenerating");
                x.refresh().context("Can't refresh cache")?;
            }
            Ok(())
        } else {
            Err(anyhow!("Can't lock mutex"))
        }
    }

    /// Refreshes a collection cache if necessary
    fn refresh_partial_cache_if_outdated(&mut self, collection_id: &str) -> Result<()> {
        if let Ok(mut x) = self.media_cache.lock() {
            if x.collection_is_outdated(collection_id, &self.partial_cache_timeout) {
                info!(
                    "Partial media cache for {} is outdated! Regenerating",
                    collection_id
                );
                x.refresh_collection(collection_id)
            } else {
                Ok(())
            }
        } else {
            warn!("Can't lock mutex");
            Err(anyhow!("Can't lock mutex"))
        }
    }

    /// Tries to get a file path corresponding to an ID
    fn try_fetch_file(&self, id: &str, file_type: &str) -> Result<Option<String>> {
        if let Ok(x) = self.media_cache.lock() {
            Ok(x.get(id, file_type))
        } else {
            Err(anyhow!("Can't lock mutex"))
        }
    }

    /// Tries to fetch a
    fn fetch_file(
        &mut self,
        id: String,
        file_type: &str,
        force_partial_refresh: bool,
    ) -> Result<Response<Body>> {
        if self.refresh_cache_if_outdated().is_err() {
            error!("Can't access media cache mutex!");
            return internal_server_error();
        }
        match self.try_fetch_file(&id, file_type) {
            Ok(r) => match r {
                Some(p) => {
                    info!("{} file for id {} found", &file_type, &id);
                    if let Ok(f) = File::open(p) {
                        let mut reader = BufReader::new(f);
                        let mut buf: Vec<u8> = vec![];
                        reader
                            .read_to_end(&mut buf)
                            .context("Can't read media file")?;
                        binary_content_response(buf)
                    } else {
                        warn!("{} file for id {} not found, a respective entry in the lookup table exists however", &file_type, &id);
                        not_found()
                    }
                }
                None if force_partial_refresh => {
                    let _ = self.refresh_partial_cache_if_outdated(id.split_at(7).0);
                    self.fetch_file(id, file_type, false)
                }

                None => {
                    warn!("{} file for id {} not found", &file_type, &id);
                    not_found()
                }
            },
            Err(_) => internal_server_error(),
        }
    }
}

impl Service<Request<Body>> for Svc {
    type Response = Response<Body>;
    type Error = anyhow::Error;
    #[allow(clippy::type_complexity)]
    type Future = Pin<Box<dyn Future<Output = Result<Self::Response, Self::Error>> + Send>>;

    fn poll_ready(&mut self, _: &mut Context) -> Poll<Result<(), Self::Error>> {
        Poll::Ready(Ok(()))
    }

    fn call(&mut self, req: Request<Body>) -> Self::Future {
        let res = match Endpoint::new(req.uri().path()) {
            Endpoint::Health => ok(),
            Endpoint::Media(id) => self.fetch_file(id.to_owned(), "media", true),
            Endpoint::Thumbnail(id) => self.fetch_file(id.to_owned(), "thumbnail", true),
            Endpoint::Refresh => {
                if let Ok(mut l) = self.media_cache.lock() {
                    match l.refresh() {
                        Ok(_) => {
                            info!("Cache refreshed as requested");
                            ok()
                        }
                        _ => {
                            warn!("Cache refreshing failed!");
                            internal_server_error()
                        }
                    }
                } else {
                    error!("Couldn't lock cache mutex!");
                    internal_server_error()
                }
            }
            Endpoint::Summary(id) => {
                if let Some(collection_id) = id {
                    if let Ok(x) = self.media_cache.lock() {
                        if let Some(metadata_count) = x.collection_metadata_file_size(collection_id)
                        {
                            let collection_dissemination_copies_size =
                                x.collection_dissemination_copies_size(collection_id);
                            let collection_thumbnails_size =
                                x.collection_thumbnails_size(collection_id);
                            let body = format!("{{\"metadata_files\": {}, \"dissemination_copies\": {}, \"thumbnails\": {}}}",
                                               metadata_count, collection_dissemination_copies_size, collection_thumbnails_size);
                            json_response(body)
                        } else {
                            not_found()
                        }
                    } else {
                        error!("Couldn't lock cache mutex!");
                        internal_server_error()
                    }
                } else if let Ok(x) = self.media_cache.lock() {
                    let body = format!("{{\"metadata_files\": {}, \"dissemination_copies\": {}, \"thumbnails\": {}}}",
                                       x.metadata_file_size(), x.dissemination_copies_size(), x.thumbnails_size());
                    json_response(body)
                } else {
                    error!("Couldn't lock cache mutex!");
                    internal_server_error()
                }
            }
            Endpoint::Collections => {
                if let Ok(l) = self.media_cache.lock() {
                    let body = format!("[\"{}\"]", l.get_collections().join("\",\""));
                    json_response(body)
                } else {
                    error!("Couldn't lock cache mutex!");
                    internal_server_error()
                }
            }
            Endpoint::Unknown(p) => {
                info!("No route on /{}", p);
                bad_request()
            }
            Endpoint::Error(err) => {
                warn!("Bad request {}", &err);
                bad_request()
            }
        };

        Box::pin(async { res })
    }
}

/// Returns a 500 response
fn internal_server_error() -> Result<Response<Body>> {
    Response::builder()
        .status(StatusCode::INTERNAL_SERVER_ERROR)
        .body("Internal Server Error".into())
        .context("Can't build response")
}

/// Returns a 404 response
fn not_found() -> Result<Response<Body>> {
    Response::builder()
        .status(StatusCode::NOT_FOUND)
        .body("No file found".into())
        .context("Can't build response")
}

/// Returns a 400 response
fn bad_request() -> Result<Response<Body>> {
    Response::builder()
        .status(StatusCode::BAD_REQUEST)
        .body("Bad request".into())
        .context("Can't build response")
}

/// Returns a binary object
fn binary_content_response(binary_content: Vec<u8>) -> Result<Response<Body>> {
    Response::builder()
        .header(header::CONTENT_TYPE, "application/octet-stream")
        .body(Body::from(binary_content))
        .context("Can't build response")
}

/// Returns a JSON object
fn json_response(body: String) -> Result<Response<Body>> {
    Response::builder()
        .status(StatusCode::OK)
        .header("Content-Type", "application/json")
        .header("Content-Length", body.as_bytes().len())
        .body(body.into())
        .context("Can't build response")
}

/// Returns a 200 response
fn ok() -> Result<Response<Body>> {
    Response::builder()
        .status(StatusCode::OK)
        .body("Ok".into())
        .context("Can't build response")
}
