/*
 * Media File Distributor
 * Copyright (C) 2021  Memoriav
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

use crate::{CustomCollectionMappings, CustomMappings};
use anyhow::anyhow;
use anyhow::{Context, Result};
use log::{debug, warn};
use std::collections::HashMap;
use std::fs;
use std::path::{Path, PathBuf};
use std::time::{Duration, Instant};

/// Represents a file path cache for a single collection (i.e. "record set")
#[derive(Debug)]
pub struct Collection {
    /// Index of dissemination copies ("digital objects") in collection
    dissemination_copies: HashMap<String, String>,
    /// Index of thumbnail files in collection
    thumbnails: HashMap<String, String>,
    /// Number of dissemination copies in collection
    num_dissemination_copies: usize,
    /// Number of thumbnails in collection
    num_thumbnails: usize,
    /// Number of metadata files in collection
    num_metadata_files: usize,
    /// File path to collection
    base_path: String,
    /// Creation instance of collection. Set to [`std::time::Instant::now()`] when collection has been updated.
    created_on: Instant,
}

impl Collection {
    /// Creates a new collection
    pub fn new(base_path: String, custom_mappings: Option<CustomCollectionMappings>) -> Self {
        let base = Path::new(&base_path);
        let parse = |r: &str| {
            let path = base.join(r);
            let path = path.as_path();
            let path_as_str = path.to_str().unwrap_or_else(|| {
                warn!("Can't parse path");
                "<unknown>"
            });
            if let false = path.exists() {
                debug!("Directory does not exist: {}", path_as_str);
                HashMap::new()
            } else if let Ok(index) = Collection::scan_media_folder(path) {
                debug!(
                    "{} {} files parsed in directory {}",
                    index.len(),
                    r,
                    path_as_str
                );
                index
            } else {
                warn!("Indexing of files in path {} failed", path_as_str);
                HashMap::new()
            }
        };
        let mut dissemination_copies = parse("media");
        let num_dissemination_copies = dissemination_copies.len();
        let mut thumbnails = parse("thumbnails");
        let num_thumbnails = thumbnails.len();
        if let Some(ids) = custom_mappings {
            if let Some(media_ids) = ids.get("media") {
                let media_ids_cloned = media_ids
                    .to_owned()
                    .into_iter()
                    .map(|v| {
                        (
                            v.0,
                            base.join("media").join(v.1).to_str().unwrap().to_string(),
                        )
                    })
                    .collect::<HashMap<String, String>>();
                dissemination_copies.extend(media_ids_cloned);
            }
            if let Some(thumbnail_ids) = ids.get("thumbnails") {
                let thumbnail_ids_cloned = thumbnail_ids
                    .to_owned()
                    .into_iter()
                    .map(|v| {
                        (
                            v.0,
                            base.join("thumbnails")
                                .join(v.1)
                                .to_str()
                                .unwrap()
                                .to_string(),
                        )
                    })
                    .collect::<HashMap<String, String>>();
                thumbnails.extend(thumbnail_ids_cloned);
            }
        }
        Collection {
            dissemination_copies,
            thumbnails,
            num_dissemination_copies,
            num_thumbnails,
            num_metadata_files: Collection::count_metadata_files(base).unwrap_or_else(|_| {
                warn!("No metadata files found in {}", &base_path);
                0
            }),
            base_path,
            created_on: Instant::now(),
        }
    }

    /// Extracts file ID from file path
    fn create_id(path: &PathBuf) -> Result<(String, String)> {
        let record_id = path
            .file_stem()
            .with_context(|| {
                warn!("Can't extract file stem");
                "Can't extract file stem"
            })?
            .to_str()
            .with_context(|| {
                warn!("Can't convert file stem to str");
                "Can't convert file stem to str"
            })?;
        let path = path.to_str().with_context(|| {
            warn!("Can't convert path to str");
            "Can't read path!"
        })?;
        Ok((record_id.replace(" ", "_"), path.to_owned()))
    }

    /// Counts number of metadata files in collection
    fn count_metadata_files(dir: &Path) -> Result<usize> {
        let mut file_counter: usize = 0;
        for entry in fs::read_dir(&dir)? {
            let entry = entry?;
            let path = entry.path();
            let extension = path.extension().and_then(|e| e.to_str());
            if extension.is_some() && extension.unwrap() == "xml" {
                file_counter += 1;
            }
        }
        Ok(file_counter)
    }

    /// Scans directory for media files and returns an index of found files
    fn scan_media_folder(dir: &Path) -> Result<HashMap<String, String>> {
        let mut file_index: HashMap<String, String> = HashMap::new();
        for entry in fs::read_dir(&dir).context("Can't read dir")? {
            let entry = entry?;
            let path = entry.path();
            if !path.is_dir() {
                let (key, value) = Collection::create_id(&path).context("Couldn't index file")?;
                file_index.insert(key, value);
            }
        }
        Ok(file_index)
    }

    /// Returns the file path for a certain ID - media type combination, where the latter is either `media` or `thumbnail`
    pub fn get_file_path(&self, record_id: &str, media_type: &str) -> Option<String> {
        if media_type == "media" {
            self.dissemination_copies
                .get(record_id)
                .map(|id| id.to_owned())
        } else if media_type == "thumbnail" {
            self.thumbnails.get(record_id).map(|id| id.to_owned())
        } else {
            None
        }
    }

    /// Updates the media index for a certain directory
    pub fn update_media_index(&mut self, dir: &Path) -> Result<()> {
        if !dir.exists() {
            debug!(
                "{} does not exist therefore skipping indexing of it",
                dir.to_str().unwrap_or("")
            );
            return Ok(());
        }
        if dir.ends_with("media") {
            self.dissemination_copies =
                Collection::scan_media_folder(dir).context("Couldn't scan folder media")?;
        } else if dir.ends_with("thumbnails") {
            self.thumbnails =
                Collection::scan_media_folder(dir).context("Couldn't scan folder thumbnails")?;
        }
        self.created_on = Instant::now();
        Ok(())
    }

    /// Checks if the cache is outdated according to `outdated_after`
    pub fn is_outdated(&self, outdated_after: &Duration) -> bool {
        let now = Instant::now();
        now.duration_since(self.created_on) >= *outdated_after
    }

    /// Returns index size for dissemination copies (i.e. the total of cached dissemination copies)
    pub fn dissemination_copies_size(&self) -> usize {
        self.num_dissemination_copies
    }

    /// Returns index size for thumbnails (i.e. the total of cached thumbnails)
    pub fn thumbnails_size(&self) -> usize {
        self.num_thumbnails
    }

    /// Retuns number of metadata files in collection
    pub fn metadata_files(&self) -> usize {
        self.num_metadata_files
    }
}

/// Caches all collections found in the indicated directory tree.
#[derive(Debug)]
pub struct MediaFileCache {
    /// Collection index
    collections: HashMap<String, Collection>,
    /// Ignored collection folders
    ignored_folders: Vec<String>,
    /// Mappings for special file names
    custom_mappings: Option<CustomMappings>,
    /// Path to root directory
    base_path: String,
    /// Creation instance of cache. Set to [`std::time::Instant::now()`] when cache has been updated
    created_on: Instant,
}

impl MediaFileCache {
    /// Creates a new instance
    pub fn new(
        base_path: String,
        ignore_collection_paths: Option<Vec<String>>,
        custom_mappings: Option<CustomMappings>,
    ) -> Self {
        let ignored_folders = if ignore_collection_paths.is_some() {
            ignore_collection_paths.unwrap()
        } else {
            vec![]
        };
        let collections = if let Ok(index) = MediaFileCache::scan_collection_folders(
            Path::new(&base_path),
            &ignored_folders,
            &custom_mappings,
        ) {
            index
        } else {
            warn!("Collection indexing failed!");
            HashMap::new()
        };
        MediaFileCache {
            collections,
            ignored_folders,
            custom_mappings,
            base_path,
            created_on: Instant::now(),
        }
    }

    /// Scans the root path for collections and returns an index of collection IDs and their files
    fn scan_collection_folders(
        dir: &Path,
        ignored_folders: &Vec<String>,
        custom_mappings: &Option<CustomMappings>,
    ) -> Result<HashMap<String, Collection>> {
        let mut file_index: HashMap<String, Collection> = HashMap::new();
        for entry in fs::read_dir(&dir).context("Can't read dir")? {
            let entry = entry?;
            let path = entry.path();
            if path.is_dir()
                && entry
                    .file_name()
                    .into_string()
                    .and_then(|s| Ok(!ignored_folders.contains(&s)))
                    .unwrap_or(false)
            {
                let path_as_string = path.to_str().unwrap().to_owned();
                let dir_name = path
                    .file_name()
                    .context("Can't extract file name")?
                    .to_str()
                    .context("Can't convert OsStr to str")?
                    .to_owned();
                let custom_collection_mappings =
                    if let Some(m) = custom_mappings.as_ref().and_then(|m| m.get(&dir_name)) {
                        Some(m.clone())
                    } else {
                        None
                    };
                file_index.insert(
                    dir_name,
                    Collection::new(path_as_string, custom_collection_mappings),
                );
            }
        }
        Ok(file_index)
    }

    /// Checks if the cache is outdated according to `outdated_after`
    pub fn is_outdated(&self, outdated_after: &Duration) -> bool {
        let now = Instant::now();
        now.duration_since(self.created_on) >= *outdated_after
    }

    /// Checks if cache for a certain collection is outdated
    pub fn collection_is_outdated(&self, collection_id: &str, outdated_after: &Duration) -> bool {
        if let Some(c) = self.collections.get(collection_id) {
            c.is_outdated(outdated_after)
        } else {
            false
        }
    }

    /// Returns number of all contained dissemination copies
    pub fn dissemination_copies_size(&self) -> usize {
        let mut counter: usize = 0;
        for collection in self.collections.values() {
            counter += collection.dissemination_copies_size();
        }
        counter
    }

    ///  Returns number of dissemination copies in a certain collection
    pub fn collection_dissemination_copies_size(&self, collection_id: &str) -> usize {
        match self.collections.get(collection_id) {
            Some(c) => c.dissemination_copies_size(),
            None => 0,
        }
    }

    /// Returns number of all contained thumbnails
    pub fn thumbnails_size(&self) -> usize {
        let mut counter: usize = 0;
        for collection in self.collections.values() {
            counter += collection.thumbnails_size();
        }
        counter
    }

    /// Returns number of thumbnails in a certain collection
    pub fn collection_thumbnails_size(&self, collection_id: &str) -> usize {
        match self.collections.get(collection_id) {
            Some(c) => c.thumbnails_size(),
            None => 0,
        }
    }

    /// Returns number of all contained metadata files
    pub fn metadata_file_size(&self) -> usize {
        let mut counter: usize = 0;
        for collection in self.collections.values() {
            counter += collection.metadata_files();
        }
        counter
    }

    /// Returns number of metadata files in a certain collection
    pub fn collection_metadata_file_size(&self, collection_id: &str) -> Option<usize> {
        self.collections
            .get(collection_id)
            .map(|c| c.metadata_files())
    }

    /// Returns a list of all collection IDs
    pub fn get_collections(&self) -> Vec<String> {
        self.collections
            .keys()
            .map(|k| k.to_owned())
            .collect::<Vec<String>>()
    }

    /// Returns file path for an ID and a media type, where latter can be either `media` or `thumbnail`
    pub fn get(&self, id: &str, media_type: &str) -> Option<String> {
        let collection_id = id.split_at(7).0;
        self.collections
            .get(collection_id)
            .and_then(|c| c.get_file_path(id.split_at(8).1, media_type))
    }

    /// Refreshes the entire cache
    pub fn refresh(&mut self) -> Result<()> {
        self.collections = MediaFileCache::scan_collection_folders(
            Path::new(&self.base_path),
            &self.ignored_folders,
            &self.custom_mappings,
        )
        .context("Cache refreshing failed")?;
        self.created_on = Instant::now();
        Ok(())
    }

    /// Refreshes the cache for a collection exclusively
    pub fn refresh_collection(&mut self, collection_id: &str) -> Result<()> {
        if let Some(c) = self.collections.get_mut(collection_id) {
            let collection_path = Path::new(&self.base_path);
            let collection_path = collection_path.join(collection_id);
            let collection_path = collection_path.as_path();
            c.update_media_index(collection_path.join("media").as_path())
                .and_then(|_| c.update_media_index(collection_path.join("thumbnails").as_path()))
        } else {
            warn!("Collection id {} not found", collection_id);
            Err(anyhow!("Collection id not found"))
        }
    }
}
