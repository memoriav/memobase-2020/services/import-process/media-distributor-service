/*
 * Media File Distributor
 * Copyright (C) 2021  Memoriav
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

//! A general comment

mod endpoints;
mod media_folder_utils;
mod service;

#[macro_use]
extern crate lazy_static;

use crate::media_folder_utils::MediaFileCache;
use crate::service::Svc;
use anyhow::{Context, Result};
use hyper::service::make_service_fn;
use hyper::{Error, Server};
use log::info;
use serde::Deserialize;
use std::collections::HashMap;
use std::env;
use std::fs::File;
use std::io::prelude::*;
use std::net::SocketAddr;
use std::str::FromStr;
use std::sync::{Arc, Mutex};
use tokio::time::Duration;

type CustomCollectionMappings = HashMap<String, HashMap<String, String>>;
type CustomMappings = HashMap<String, CustomCollectionMappings>;

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error + Send + Sync>> {
    let args = env::args();
    let config_dir = args.skip(1).take(1).collect::<String>();

    let config = parse_config_file(&config_dir)
        .context(format!("Can't parse config on {} file!", &config_dir))?;

    log4rs::init_file(&config.logging_config, Default::default()).unwrap();

    let addr = SocketAddr::from_str(&config.host).context("Host name invalid")?;
    info!("Server starts on {}", &config.host);

    info!(
        "Reading in media files starting with path {}",
        &config.base_path
    );
    let media_cache = MediaFileCache::new(
        config.base_path.clone(),
        config.ignore_collection_paths.clone(),
        config.mappings.clone(),
    );
    info!(
        "Done reading in media files. Found {} metadata files, {} media files and {} thumbnails",
        media_cache.metadata_file_size(),
        media_cache.dissemination_copies_size(),
        media_cache.thumbnails_size()
    );
    let media_cache = Arc::new(Mutex::new(media_cache));

    let cache_timeout = Duration::new(config.cache_timeout.unwrap_or(7200), 0);
    let partial_cache_timeout = Duration::new(config.partial_cache_timeout.unwrap_or(60), 0);
    info!("Setting refresh period to {}s", cache_timeout.as_secs());
    info!(
        "Setting partial refresh period to {}s",
        partial_cache_timeout.as_secs()
    );

    let server = Server::bind(&addr).serve(make_service_fn(move |_| {
        let cloned_base_dir = config.base_path.clone();
        let cloned_media_cache = media_cache.clone();
        async move {
            Ok::<_, Error>(Svc {
                media_cache: cloned_media_cache,
                base_dir: cloned_base_dir,
                cache_timeout,
                partial_cache_timeout,
            })
        }
    }));

    if let Err(e) = server.await {
        eprintln!("server error: {}", e);
    }
    Ok(())
}

/// Parses a provided configuration file
pub fn parse_config_file(path: &str) -> Result<Config> {
    let mut file = File::open(path)?;
    let mut contents = String::new();
    file.read_to_string(&mut contents)?;
    toml::from_str(&contents).context("Parsing of TOML config file failed")
}

/// Internal configuration representation
#[derive(Deserialize)]
pub struct Config {
    /// path to logging configuration
    pub logging_config: String,
    /// host:port tuple of webserver
    pub host: String,
    /// Path to root directory of media file tree
    pub base_path: String,
    /// Ignored paths in root directory
    pub ignore_collection_paths: Option<Vec<String>>,
    /// Timeout (in seconds) after which a full cache update is enforced
    pub cache_timeout: Option<u64>,
    /// Timeout (in seconds) after which a collection cache is updated if a query for a certain ID failed
    pub partial_cache_timeout: Option<u64>,
    /// Mappings for special file names
    pub mappings: Option<CustomMappings>,
}
