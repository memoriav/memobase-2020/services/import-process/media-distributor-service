/*
 * Media File Distributor
 * Copyright (C) 2021  Memoriav
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
use crate::endpoints::Endpoint::*;
use anyhow::{anyhow, Result};
use regex::Regex;

/// Provided endpoints
pub enum Endpoint<'a> {
    /// `/collections` endpoint: Provides a list of available collections
    Collections,
    /// `/media` endpoint: Returns a file if a valid id is provided
    Media(&'a str),
    /// `/health` endpoint: Always returns ok
    Health,
    /// `/refresh` endpoint: Refreshes the entire cache
    Refresh,
    /// `/summary` endpoint: Provides some numbers about a collection or the whole collection tree
    Summary(Option<&'a str>),
    /// `/thumbnail` endpoint: Returns a file if a valid id is provided
    Thumbnail(&'a str),
    /// If endpoint is unknown
    Unknown(&'a str),
    /// If something is wrong with the request
    Error(String),
}

impl<'a> Endpoint<'a> {
    pub fn new(path: &'a str) -> Endpoint<'a> {
        let endpoint = path.split('/').nth(1);
        match endpoint {
            Some("collections") => Collections,
            Some("media") => match Self::extract_id(path) {
                Ok(id) => Media(id),
                Err(e) => Error(e.to_string()),
            },
            Some("health") => Health,
            Some("refresh") => Refresh,
            Some("summary") => match Self::extract_collection(path) {
                Ok(collection) => Summary(collection),
                Err(e) => Error(e.to_string()),
            },
            Some("thumbnail") => match Self::extract_id(path) {
                Ok(id) => Thumbnail(id),
                Err(e) => Error(e.to_string()),
            },
            Some(p) => Unknown(p),
            _ => Unknown(""),
        }
    }

    fn extract_id(path: &str) -> Result<&str> {
        lazy_static! {
            static ref ID_PATTERN: Regex = Regex::new(r"^[[:alpha:]]{3}-\d{3}-.+").unwrap();
        }
        if let Some(id) = path.split('/').nth(2) {
            if ID_PATTERN.is_match(id) {
                Ok(id)
            } else {
                Err(anyhow!("Record id syntactically invalid"))
            }
        } else {
            Err(anyhow!("No record id indicated"))
        }
    }

    fn extract_collection(path: &str) -> Result<Option<&str>> {
        lazy_static! {
            static ref COLLECTION_PATTERN: Regex = Regex::new(r"^[[:alpha:]]{3}-\d{3}$").unwrap();
        }
        if let Some(id) = path.split('/').nth(2) {
            if COLLECTION_PATTERN.is_match(id) {
                Ok(Some(id))
            } else {
                Err(anyhow!("Collection id syntactically invalid"))
            }
        } else {
            Ok(None)
        }
    }
}
