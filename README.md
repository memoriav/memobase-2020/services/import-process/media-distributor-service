# Media File Distributor

A small service which provides access to media files hosted on the Memobase sFTP server without requiring the client to install an additional sFTP client library.

At the moment the following endpoints are supported:

* `/health`: Simply returns a 200 response. Use it as a very lightweight means
  to check if the service is still running.
* `/media/<record-id>`: Fetch a media file with the respective id. The id consists of the recordSetId (a three-letter code and a three-digit sequential number) as well as the proper id of the record. E.g. `baz-001-MEI_67473`.
* `/thumbnail/<record-id>`: The same for thumbnails (media files in the `thumbnails` directory)
* `/refresh`: Refresh the file cache. This happens also automatically after a predefined duration (see below).
* `/summary/<collection-id>`: Gives a quantitative summary of the collection
* `/collections`: Shows the IDs of all available collections

## Installation

You need Rust and Cargo for compilation (see [here](https://rustup.rs/) for instructions).

```sh
git clone https://gitlab.switch.ch/memoriav/memobase-2020/services/import-process/media-file-distributor.git
cd media-file-distributor
rustup target install x86_64-unknown-linux-musl # We use this target to avoid glibc as prerequisite. Of course this isn't strictly mandatory
cargo build --target x86_64-unknown-linux-musl --release # After compilation you should find a binary in ./target/x86_64-unknown-linux-musl/release/
strip target/x86_64-unknown-linux-musl/release/media-file-distributor # Reduce the file size a bit; this isn't mandatory
```

Afterwards, use the example configuration (`config.example.toml`) as template for your own configuration. See description in the file for details.

## Usage

Start the application by giving the path to your configuration file, e.g.

```shell
./media-file-distributor config.toml
```

### Logging

Use the `log4rs.example.yml` as a basis for logging configuration. See
[here](https://docs.rs/log4rs/1.0.0/log4rs/) for further instructions. Don't
forget to indicate the path to the logging configuration file in the main
config file if you change the name!

## File considerations

The server assumes a file tree according to this scheme:

```shell
.
├── <recordSet-1>
│   └── thumbnails
│       ├── <file-1>
│       ├── <file-2>
│       └── <file-3>
├── <recordSet-2>
│   ├── media
│       ├── <file-1>
│       ├── <file-1>
│       └── <...>
│   └── thumbnails
│       ├── <file-1>
│       └── <file-2>
├── <...>
│   └── media
│       ├── <file-1>
│       └── <file-2>
```

Importantly, the `media` and `thumbnails` directory have to be direct subfolders of the recordSet directories and must directly contain the media files.

When generating the IDs for the files, whitespace in filenames are replaced with underscored (`_`).

If a file name does not match with the id, you can define a custom mapping. See example config for details.
